"use strict";

/* jVectorMap SVG map - initialized from Angular (see dashboard-app.js) */
var initJVectorMap = (function ($) {

    var $map = $('#world-map-gdp');

    return function(dataset) {
        $map.children().remove();
        $map.vectorMap({
            map: 'world_mill_en',
            backgroundColor: 'transparent',
            zoomOnScroll: false,
            series: {
                regions: [{
                    values: dataset,
                    scale: ['#dcb893', '#c85a1e'],
                    normalizeFunction: 'polynomial'
                }]
            },
            focusOn: {
                x: 0.55,
                y: 0.33,
                scale: 5
            },
            onRegionTipShow: function(e, el, code) {
                el.html(el.html() + ' (GDP - ' + dataset[code] + ')');
            }
        });
    };

})(jQuery);

/* Mock-ups, dummy data, no dynamics */
// TODO: Add these to Angular, connect them to actual data via the node.js app
(function($) {

    var initJQuerySparklines = function() {
        /* jQuery Sparklines */
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
            type: 'bar',
            height: '125',
            barWidth: 13,
            colorMap: {
                '7': '#a1a1a1'
            },
            barSpacing: 2,
            barColor: '#26B99A'
        });

        $(".sparkline11").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3], {
            type: 'bar',
            height: '40',
            barWidth: 8,
            colorMap: {
                '7': '#a1a1a1'
            },
            barSpacing: 2,
            barColor: '#26B99A'
        });

        $(".sparkline22").sparkline([2, 4, 3, 4, 7, 5, 4, 3, 5, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6], {
            type: 'line',
            height: '40',
            width: '200',
            lineColor: '#26B99A',
            fillColor: 'transparent',
            lineWidth: 3,
            spotColor: '#34495E',
            minSpotColor: '#34495E'
        });
    };

    var initFlot = function() {
        /* Flot */
        //define chart clolors ( you maybe add more colors if you want or flot will add it automatic )
        var chartColours = ['#c85a1e', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];

        //generate random number for charts
        randNum = function() {
            return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
        };

        var d1 = [];
        //var d2 = [];

        //here we generate data for chart
        for (var i = 0; i < 30; i++) {
            d1.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
            //    d2.push([new Date(Date.today().add(i).days()).getTime(), randNum()]);
        }

        var chartMinDate = d1[0][0]; //first day
        var chartMaxDate = d1[20][0]; //last day

        var tickSize = [1, "day"];
        var tformat = "%d/%m/%y";

        //graph options
        var options = {
            grid: {
                show: true,
                aboveData: true,
                color: "#3f3f3f",
                labelMargin: 10,
                axisMargin: 0,
                borderWidth: 0,
                borderColor: null,
                minBorderMargin: 5,
                clickable: true,
                hoverable: true,
                autoHighlight: true,
                mouseActiveRadius: 100
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    lineWidth: 2,
                    steps: false
                },
                points: {
                    show: true,
                    radius: 4.5,
                    symbol: "circle",
                    lineWidth: 3.0
                }
            },
            legend: {
                position: "ne",
                margin: [0, -25],
                noColumns: 0,
                labelBoxBorderColor: null,
                labelFormatter: function(label, series) {
                    // just add some space to labes
                    return label + '&nbsp;&nbsp;';
                },
                width: 40,
                height: 1
            },
            colors: chartColours,
            shadowSize: 0,
            tooltip: true, //activate tooltip
            tooltipOpts: {
                content: "%s: %y.0",
                xDateFormat: "%d/%m",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",
                minTickSize: tickSize,
                timeformat: tformat,
                min: chartMinDate,
                max: chartMaxDate
            }
        };
        var plot = $.plot($("#placeholder33x"), [{
            label: "Sales evolution",
            data: d1,
            lines: {
                fillColor: "rgba(200, 90, 30, 0.12)"
            }, //#96CA59 rgba(150, 202, 89, 0.42)
            points: {
                fillColor: "#fff"
            }
        }], options);
    };

    var initDoughnutCharts = function() {
        /* Doughnut Chart */
        var options = {
            legend: true,
            responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
            type: 'doughnut',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    "Ultimate",
                    "Connect",
                    "Pro 2",
                    "One",
                    "RC"
                ],
                datasets: [{
                    data: [30, 10, 20, 10, 30],
                    backgroundColor: [
                        "#c85a1e",
                        "#3498DB",
                        "#E74C3C",
                        "#9B59B6",
                        "#26B99A"
                    ],
                    hoverBackgroundColor: [
                        "#d08e6a",
                        "#49A9EA",
                        "#E95E4F",
                        "#B370CF",
                        "#36CAAB"
                    ]
                }]
            },
            options: options
        });
    };

    var initEasyPieChart = function() {

        /* easy-pie-chart */
        $('.easy-chart').easyPieChart({
            easing: 'easeOutBounce',
            lineWidth: '6',
            barColor: '#75BCDD',
            onStep: function(from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });
        var chart = window.chart = $('.easy-chart').data('easyPieChart');
        $('.js_update').on('click', function() {
            chart.update(Math.random() * 200 - 100);
        });

        //hover and retain popover when on popover content
        var originalLeave = $.fn.popover.Constructor.prototype.leave;
        $.fn.popover.Constructor.prototype.leave = function(obj) {
            var self = obj instanceof this.constructor ?
                obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
            var container, timeout;

            originalLeave.call(this, obj);

            if (obj.currentTarget) {
                container = $(obj.currentTarget).siblings('.popover');
                timeout = self.timeout;
                container.one('mouseenter', function() {
                    //We entered the actual popover – call off the dogs
                    clearTimeout(timeout);
                    //Let's monitor popover content instead
                    container.one('mouseleave', function() {
                        $.fn.popover.Constructor.prototype.leave.call(self, self);
                    });
                });
            }
        };

        $('body').popover({
            selector: '[data-popover]',
            trigger: 'click hover',
            delay: {
                show: 50,
                hide: 400
            }
        });

    };

    // After page is ready
    $(function() {

        initJQuerySparklines();

        initFlot();

        initDoughnutCharts();

        initEasyPieChart();

    });

})(jQuery);