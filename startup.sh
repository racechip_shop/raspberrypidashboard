#!/usr/bin/env bash

# Executed at every boot. See 'dashboard' service.
# sudo systemctl enable dashboard.service
# sudo systemctl start dashboard.service
# sudo systemctl stop dashboard.service
# sudo systemctl status dashboard.service
# sudo systemctl disable dashboard.service

cd ~/Public/
/usr/bin/gulp > ~/Public/logs/gulp.log 2>&1 &
/usr/bin/node app.js > ~/Public/logs/app.log 2>&1 &
