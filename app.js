/* Node.js app that handles communication to the Magento website */

"use strict";


/* --- Includes --- */

// App configuration
var fs = require('fs');
var conf = JSON.parse(
    fs.readFileSync('./config.json')
);

// Checking the server status
var request = require('request');
var checkServerOpts = {
    url: conf.checkServerURL,
    timeout: conf.checkServerTimeout
};

// Express server
var express = require('express');
var app = express();

// LEDs alert control module
if(conf.useHardwareLights) {
    var alertLights = require('./shift.js').DashAlert;
}


/* --- Application --- */

// Handler for the maps data route
var getMapsData = function (req, res, next) {

    require('./magento_soap')(function (err, customers, stores) {
        // This code runs once the response is ready
        var customersList = customers,
            storesList = stores,
            countriesPercentage = [],
            mapData={
                "AF": 0,
                "AL": 0,
                "DZ": 0,
                "AO": 0,
                "AG": 0,
                "AR": 0,
                "AM": 0,
                "AU": 0,
                "AT": 0,
                "AZ": 0,
                "BS": 0,
                "BH": 0,
                "BD": 0,
                "BB": 0,
                "BY": 0,
                "BE": 0,
                "BZ": 0,
                "BJ": 0,
                "BT": 0,
                "BO": 0,
                "BA": 0,
                "BW": 0,
                "BR": 0,
                "BN": 0,
                "BG": 0,
                "BF": 0,
                "BI": 0,
                "KH": 0,
                "CM": 0,
                "CA": 0,
                "CV": 0,
                "CF": 0,
                "TD": 0,
                "CL": 0,
                "CN": 0,
                "CO": 0,
                "KM": 0,
                "CD": 0,
                "CG": 0,
                "CR": 0,
                "CI": 0,
                "HR": 0,
                "CY": 0,
                "CZ": 0,
                "DK": 0,
                "DJ": 0,
                "DM": 0,
                "DO": 0,
                "EC": 0,
                "EG": 0,
                "SV": 0,
                "GQ": 0,
                "ER": 0,
                "EE": 0,
                "ET": 0,
                "FJ": 0,
                "FI": 0,
                "FR": 0,
                "GA": 0,
                "GM": 0,
                "GE": 0,
                "DE": 0,
                "GH": 0,
                "GR": 0,
                "GD": 0,
                "GT": 0,
                "GN": 0,
                "GW": 0,
                "GY": 0,
                "HT": 0,
                "HN": 0,
                "HK": 0,
                "HU": 0,
                "IS": 0,
                "IN": 0,
                "ID": 0,
                "IR": 0,
                "IQ": 0,
                "IE": 0,
                "IL": 0,
                "IT": 0,
                "JM": 0,
                "JP": 0,
                "JO": 0,
                "KZ": 0,
                "KE": 0,
                "KI": 0,
                "KR": 0,
                "UNDEFINED": 0,
                "KW": 0,
                "KG": 0,
                "LA": 0,
                "LV": 0,
                "LB": 0,
                "LS": 0,
                "LR": 0,
                "LY": 0,
                "LT": 0,
                "LU": 0,
                "MK": 0,
                "MG": 0,
                "MW": 0,
                "MY": 0,
                "MV": 0,
                "ML": 0,
                "MT": 0,
                "MR": 0,
                "MU": 0,
                "MX": 0,
                "MD": 0,
                "MN": 0,
                "ME": 0,
                "MA": 0,
                "MZ": 0,
                "MM": 0,
                "NA": 0,
                "NP": 0,
                "NL": 0,
                "NZ": 0,
                "NI": 0,
                "NE": 0,
                "NG": 0,
                "NO": 0,
                "OM": 0,
                "PK": 0,
                "PA": 0,
                "PG": 0,
                "PY": 0,
                "PE": 0,
                "PH": 0,
                "PL": 0,
                "PT": 0,
                "QA": 0,
                "RO": 0,
                "RU": 0,
                "RW": 0,
                "WS": 0,
                "ST": 0,
                "SA": 0,
                "SN": 0,
                "RS": 0,
                "SC": 0,
                "SL": 0,
                "SG": 0,
                "SK": 0,
                "SI": 0,
                "SB": 0,
                "ZA": 0,
                "ES": 0,
                "LK": 0,
                "KN": 0,
                "LC": 0,
                "VC": 0,
                "SD": 0,
                "SR": 0,
                "SZ": 0,
                "SE": 0,
                "CH": 0,
                "SY": 0,
                "TW": 0,
                "TJ": 0,
                "TZ": 0,
                "TH": 0,
                "TL": 0,
                "TG": 0,
                "TO": 0,
                "TT": 0,
                "TN": 0,
                "TR": 0,
                "TM": 0,
                "UG": 0,
                "UA": 0,
                "AE": 0,
                "GB": 0,
                "US": 0,
                "UY": 0,
                "UZ": 0,
                "VU": 0,
                "VE": 0,
                "VN": 0,
                "YE": 0,
                "ZM": 0,
                "ZW": 0
            };

        storesList.forEach(function (store) {
            var storeCustomers = customersList.filter(function (el) {
                return el.store_id == store.store_id;
            });
            var percentage = (storeCustomers.length / customersList.length) * 100;
            if(percentage > 0) {
                var obj = {
                    country: store.name,
                    percentage: percentage.toFixed(2)
                };
                countriesPercentage.push(obj);
            }

            //TODO: we need to find a new way of setting this dynamically
            if(store.code == 'de') {
                mapData['DE'] = storeCustomers.length;
            } else if(store.code == 'frc') {
                mapData['FR'] = storeCustomers.length;
            } else if(store.code == 'at') {
                mapData['AT'] = storeCustomers.length;
            } else if(store.code == 'uk') {
                mapData['GB'] = storeCustomers.length;
            } else if(store.code == 'ch') {
                mapData['CH'] = storeCustomers.length;
            }
        });

        //sort array of percentages
        countriesPercentage.sort(function(a, b) {
            return b.percentage - a.percentage  ||  a.country.localeCompare(b.country);
        });

        // Here is where the mocking takes place (this should be returned by Magento)
        var geoData = {

            mapData: mapData, // key-value object with ISO country code as key and number of customers as value
            customerData: { // customer / country relations
                countries: countriesPercentage, // array of key-value objects with the name of the country and the percentage of total customers
                customers_no: customersList.length, // total no. of customers
                countries_no: storesList.length // total no of countries we have customers from
            }

        };

        res.set('Content-Type', 'application/json');
        // Fix cross-domain AJAX request (actually same domain, different ports)
        //res.set('Access-Control-Allow-Origin', 'http://' + conf.hostName + ':' + conf.dashboardPort);
        res.set('Access-Control-Allow-Origin', '*');
        res.send(JSON.stringify(geoData));

        next();
    });
};

app.get(conf.rootApiURL + '/maps', getMapsData);

// Interval-ed function to check the server status and turn on the LEDs and alarm accordingly
(function checkServer() {
    var tStamp1 = new Date(),
        tStamp2,
        tLapse;

    // We have 7 levels for 6 LEDs and 1 red alert light
    var alertLevel = 0;

    request(checkServerOpts, function (err, res, body) {
        /* alertLevel (response times) - color LEDs:
         * 1 (t <= 500ms)  - 1 green LED
         * 2 (t > 500ms)  - 2 green LEDs
         * 3 (t > 700ms)  - 2 green + 1 yellow LEDs
         * 4 (t > 900ms)  - 2 green + 2 yellow LEDs
         * 5 (t > 1000ms) - 2 green + 2 yellow + 1 red LEDs
         * 6 (t > 1500ms) - all LEDs
         * 7 (t > 2500ms | resp error | non-200 status code) - all LEDs + the alert light (+siren :))
         */

        if (err) {
            // We have an error in the request - web server is most likely down!
            alertLevel = 7;

            console.dir(err);
        } else {
            // No error - we're getting the status code and measuring the time now
            var statusCode = res.statusCode;

            tStamp2 = new Date();
            tLapse = new Date(tStamp1 - tStamp2).getMilliseconds();

            if (statusCode == 200) {
                // Everything is fine
                if(tLapse <= 500) {
                    alertLevel = 1;
                } else if(tLapse > 500 && tLapse <= 800) {
                    alertLevel = 2;
                } else if(tLapse > 800 && tLapse <= 900) {
                    alertLevel = 3
                } else if(tLapse > 900 && tLapse <= 1000) {
                    alertLevel = 4
                } else if(tLapse > 1000 && tLapse <= 1500) {
                    alertLevel = 5
                } else if(tLapse > 1500 && tLapse <= 2500) {
                    alertLevel = 6
                } else {
                    alertLevel = 7;
                }
            } else {
                // A code different from 200 could also mean that the web server is down
                alertLevel = 7;
                console.log("[" + (tStamp2 ? tStamp2 : tStamp1) + "] ERR[check.php]: Status code: " + statusCode + " - Request took " + tLapse + "ms.");
            }
        }

        // Logging the results
        console.log("[" + (tStamp2 ? tStamp2 : tStamp1) + "] RES[check.php]: Status: " + statusCode + " - Alert level: " + alertLevel + (tLapse ? " (" + tLapse + "ms)" : ""));

        // Turning on the lights!
        if(conf.useHardwareLights) {
            alertLights(alertLevel);
        }
    });

    setTimeout(checkServer, conf.checkServerInterval);
})();

/* Turn off the LEDs and alarm on exit */
process.on('SIGINT', function() {
    console.log("Caught interrupt signal, exiting...");

    // All pins low, all lights off
    if(conf.useHardwareLights) {
        alertLights(0);
    }

    process.exit();
});


/* --- Server --- */

app.listen(conf.appPort, function () {
    console.log('App listening on: ' + conf.hostName + ':' + conf.appPort + '...');
});

