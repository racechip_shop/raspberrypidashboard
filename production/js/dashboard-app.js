/* Angular app to render the data widgets */

var app = angular.module('RCDashboard', []);

// Get the data for the vector map showing the geographical repartition of customers
app.controller('worldMapController', function($scope, $window, $http) {

    var refreshInterval = 60000; // 10 minutes

    (function refreshMap() {
        $http.get('http://localhost:5000/get_data/maps').then(
            function(response) { // success
                var d = response.data;
                // Initialize the SVG map with customer data coming from the nodejs app (which in turn comes from Magento)
                $scope.customers_countries = d.customerData.countries;
                $scope.customers_no = d.customerData.customers_no;
                $scope.countries_no = d.customerData.countries_no;

                initJVectorMap(d.mapData);
            }, function(response) { // error
                console.error("AJAX Request Error!");
            }
        );

        setTimeout(refreshMap, refreshInterval);
    })();

});