/* A library that allows simple access to 74HC595 shift registers on a Raspberry Pi using any digital I/O pins. */

"use strict";


var GPIO = require('rpio');


// Define MODES
var ALL  = -1,
    HIGH = 1,
    LOW  = 0;

// Define pins
var _SER_pin   = 22,    // pin 14 on the 75HC595 / BCM25
    _RCLK_pin  = 18,    // pin 12 on the 75HC595 / BCM24
    _SRCLK_pin = 16,    // pin 11 on the 75HC595 / BCM23
    _RLY_pin   = 13;    // pin 13 / BCM27

// is used to store states of all pins
var _registers = [];

// How many of the shift registers - you can change them with shiftRegisters method
var _number_of_shiftregisters = 1;


/* Helper functions */

function _all_pins() {

    return _number_of_shiftregisters * 8;

}

function _all(mode, execute) {

    execute = execute || true;

    var all_shr = _all_pins();

    for(var pin=0; pin<all_shr; pin++) {
        _setPin(pin, mode);
    }

    if(execute) {
        _execute();
    }

    return _registers;

}

function _setPin(pin, mode) {

    _registers[pin] = mode;

}

function _execute() {
    var all_pins = _all_pins();
    var pin_mode;

    GPIO.write(_RCLK_pin, LOW);

    for(var pin=all_pins-1; pin>=0; pin--) {
        GPIO.write(_SRCLK_pin, LOW);
        pin_mode = _registers[pin];
        GPIO.write(_SER_pin, pin_mode);
        GPIO.write(_SRCLK_pin, HIGH);
    }

    GPIO.write(_RCLK_pin, HIGH);
}


/* Initial state of the pins */
function pinsSetup() {
    GPIO.open(_SER_pin, GPIO.OUTPUT);
    GPIO.open(_RCLK_pin, GPIO.OUTPUT);
    GPIO.open(_SRCLK_pin, GPIO.OUTPUT);
}

/* Allows the user to change the default state of the shift registers outputs */
function startupMode(mode, execute) {

    execute = execute || false;

    if(mode.isInteger()) {
        if(mode == HIGH || mode == LOW) {
            _all(mode, execute)
        } else {
            // TODO: Add proper error handling
            console.log("The mode can be only HIGH or LOW or Dictionary with specific pins and modes");
            process.exit(0);
        }
    } else if(typeof mode == "object") {
        for(var pin in mode) {
            _setPin(parseInt(pin, 10), !!mode[pin]);
        }
        if(execute) {
            _execute();
        }
    } else {
        // TODO: Add proper error handling
        console.log("The mode can be only HIGH or LOW or Dictionary with specific pins and modes");
        process.exit(0);
    }

}

/* Allows the user to define the number of shift registers are connected */
function shiftRegisters(num) {

    _number_of_shiftregisters = num;
    _all(LOW);

}

/* Allows the user to set the state of a pin on the shift register */
function digitalWrite(pin, mode) {

    if(pin == ALL) {
        _all(mode)
    } else {
        if(_registers.length == 0) {
            _all(LOW);
        }

        _setPin(pin, mode);
    }

    _execute();

}

/* Used for creating a delay between commands */
function delay(millis, code) {

    // TODO: Do we still need this?
    var millis_to_seconds = parseFloat(millis) / 1000;
    window.setTimeout(code, millis);

}

// Setting all pins LOW on already on require (module load)
pinsSetup();


/* Main function that controls the LEDs and the light according to the alert level */
exports.DashAlert = function(alertLevel) {

    // Start by turning off everything
    _all(LOW);
    GPIO.write(_RLY_pin, LOW);

    // Early exit, with all pins low, if level is zero
    if(alertLevel == 0) {
        return;
    }

    var max_leds_no = 6,
        leds_no = (alertLevel >= 7 ? max_leds_no : alertLevel),
        cnt = 1;

    // Cycling through the LEDs and turning them off as needed
    for(var i=7; i>=2; i--) { // design fail: LEDs are from 7 to 2, with 7, 6 being green, 5, 4 being yellow and so on.
        digitalWrite(i, HIGH);
        if(cnt == leds_no) break;
        cnt++;
    }

    // Red turning alert light
    if(alertLevel == 7) {
        GPIO.write(_RLY_pin, HIGH);
    }

};
