var soap = require('soap');

// Configuration
var sessionId = '';
var url = 'https://racechip.de/index.php/api/v2_soap/?wsdl=1';
var args = {
    username: 'dashboard',
    apiKey: 'r4c3ch1p#board'
};

/* Parses the raw data returned by the SOAP call to the Magento API */
var parseSOAPResult = function(raw, name) {

    var result = [];

    if(typeof raw == "object") {

        var _tmp = raw[name]['item'];

        if(_tmp instanceof Array) { //if there is only one item returned _tmp will not be an array so the for loop will not work
            for(var i = 0; i < _tmp.length; i++) {
                var _item = [],
                    _record = {};

                _item = _tmp[i];

                var _key = '',
                    _value = '';

                for(_key in _item) {
                    if('$value' in _item[_key]) {
                        _value = _item[_key]['$value'];
                        _record[_key] = _value;
                    }
                }

                result.push(_record);
            }
        } else {
            var _item = _tmp,
                _record = {};

            var _key = '',
                _value = '';

            for(_key in _item) {
                if('$value' in _item[_key]) {
                    _value = _item[_key]['$value'];
                    _record[_key] = _value;
                }
            }

            result.push(_record);
        }


    }

    return result;

};

module.exports = function (callback) {

    // Create the SOAP client
    soap.createClient(url, function(err, client) {
        if (err) {
            // deal with error
            console.dir(err);
            return;
        }

        // Login into Magento
        client.login(args, function (err, result) {
            if (err) {
                // deal with error
                console.dir(err);
                return;
            }

            sessionId = result['loginReturn']['$value'];

            // Get list of stores
            client.storeList({'sessionId': sessionId}, function(err, result, raw, soapHeader) {
                if (err) {
                    // deal with error
                    console.dir(err);
                    return;
                }

                var storeData = parseSOAPResult(result, 'stores');
                storeData = storeData.filter(function (el) {
                    return el.is_active == 1;
                });

                if(storeData.length) {
                    var tStamp1 = new Date(),
                        tStamp2,
                        tLapse;

                    // Use DEBUG=node-soap node magento_soap.js to get the XML headers sent and the XML responses
                    // Use debugging in PHPStorm to inspect the results
                    // @see: http://devdocs.magento.com/guides/m1x/api/soap/introduction.html#Introduction-SOAPAPIVersionv2
                    // @see: http://devdocs.magento.com/guides/m1x/api/soap/customer/customer.list.html

                    //Create an object of null to avoid inheriting any properties at all, thus avoiding possible problems in soap parsing
                    var filter = Object.create(null);
                    var filter = {
                        'filter': {
                            'filter': [
                                {
                                    'key': 'store_id',
                                    'value': 0
                                }
                            ]
                        }
                    };

                    // Get list of customers (with ability to filter by store ID and/or country) (add 'filters:' to the parameters
                    client.customerCustomerList({'sessionId': sessionId}, function(err, result, raw, soapHeader) {
                        if (err) {
                            // deal with error
                            console.dir(err);
                            return;
                        }

                        tStamp2 = new Date();

                        tLapse = new Date(tStamp1 - tStamp2).getMilliseconds();
                        console.log("Customers request took: " + tLapse + 'ms.');

                        var customers = [];
                        customers = parseSOAPResult(result, 'storeView'); // WHYYYYY storeView, whyyyy???
                        // return customers;
                        callback(err, customers, storeData);
                    });
                }
            });
        });
    });
};

